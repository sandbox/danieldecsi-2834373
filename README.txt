Pre-requisites
--------------
The module uses PHP SDK for Facebook ads. This may have it's own requirements, please check this GitHub resository for more informations:
https://github.com/facebook/facebook-php-ads-sdk

You need a Facebook Application with Marketing API enabled. More information about this:
https://developers.facebook.com/docs/marketing-apis

Credentials
--------------
You need to add a config array into your settings.php like this:
$settings['facebook_marketing'] = [
    'app_id' => ,
    'app_secret' => '',
    'access_token' => ''
];