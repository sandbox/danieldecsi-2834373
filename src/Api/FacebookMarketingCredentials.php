<?php

namespace Drupal\facebook_marketing_api\Api;

use Drupal\Core\Site\Settings;

/**
 * Class FacebookMarketingCredentials.
 */
class FacebookMarketingCredentials {

  /**
   * Facebook Application ID.
   *
   * @var int
   */
  private $appId = NULL;

  /**
   * Facebook Application secret.
   *
   * @var string
   */
  private $appSecret = NULL;

  /**
   * Acccess token.
   *
   * @var string
   */
  private $accessToken = NULL;

  /**
   * FacebookMarketingCredentials constructor.
   *
   * Loads credentials for API usage.
   */
  public function __construct() {
    $this->load();
  }

  /**
   * Loads credentials from settings file.
   *
   * @param string $settingsKey
   *   The key under API credentials are stored in the settings file.
   *
   * @throws \InvalidArgumentException
   *   Thrown when the supplied settings key is not found, or some of them are missing or not in proper format.
   */
  public function load(string $settingsKey = 'facebook_marketing_api') {
    $settings = Settings::get($settingsKey);

    if (empty($settings)) {
      throw new \InvalidArgumentException(
        t(
          "The supplied key (%key) doesn't exists. Check you settings.php",
          ["%key" => $settingsKey]
        )
      );
    }

    if (empty($settings['app_id']) || !is_int($settings['app_id'])) {
      throw new \InvalidArgumentException(t("You didn't provide an application ID or it's invalid"));
    }
    $this->setAppId($settings['app_id']);

    if (empty($settings['app_secret']) || !is_string($settings['app_secret'])) {
      throw new \InvalidArgumentException(t("You didn't provide an application secret or it's invalid"));
    }
    $this->setAppSecret($settings['app_secret']);

    if (empty($settings['access_token']) || !is_string($settings['access_token'])) {
      throw new \InvalidArgumentException(t("You didn't provide an access token or it's invalid"));
    }
    $this->setAccessToken($settings['access_token']);
  }

  /**
   * Sets Application ID.
   *
   * @param int $appId
   *   The Application ID.
   */
  public function setAppId(int $appId) {
    $this->appId = $appId;
  }

  /**
   * Returns Application ID.
   *
   * @return int
   *   The Application ID.
   */
  public function getAppId() {
    return $this->appId;
  }

  /**
   * Sets Application secret.
   *
   * @param string $appSecret
   *   The Application secret.
   */
  public function setAppSecret(string $appSecret) {
    $this->appSecret = $appSecret;
  }

  /**
   * Returns Application secret.
   *
   * @return int
   *   The Application secret.
   */
  public function getAppSecret() {
    return $this->appSecret;
  }

  /**
   * Sets Application access token.
   *
   * @param string $acccessToken
   *   The access token.
   */
  public function setAccessToken(string $accessToken) {
    $this->accessToken = $accessToken;
  }

  /**
   * Returns Application access token.
   *
   * @return int
   *   The Application access token.
   */
  public function getAccessToken() {
    return $this->accessToken;
  }

}
