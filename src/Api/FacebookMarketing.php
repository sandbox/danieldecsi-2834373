<?php

namespace Drupal\facebook_marketing_api\Api;

use FacebookAds\Api;
use FacebookAds\Object\Campaign;

/**
 * Class FacebookMarketing.
 */
class FacebookMarketing {
  /**
   * Credentials for the API.
   *
   * @var FacebookMarketingCredentials
   */
  private $credentials = NULL;

  /**
   * FacebookMarketing contstructor.
   *
   * Load credentials and intialize API.
   */
  public function __construct() {
    $this->credentials = new FacebookMarketingCredentials();
    $this->initializeApi();
  }

  /**
   * Initializes Facebook Ads SDK.
   */
  private function initializeApi() {
    Api::init(
      $this->credentials->getAppId(),
      $this->credentials->getAppSecret(),
      $this->credentials->getAccessToken()
    );
  }

  /**
   * Get's Insights data about campaign through the API.
   *
   * @param int $campaignId
   *   ID of the Campaign.
   * @param array $fields
   *   Fields to return from Insights.
   * @param array $params
   *   Params for request.
   *
   * @return array
   *   Campaign data.
   */
  public function getInsights(int $campaignId, array $fields = [], array $params = []) {
    $campaign = new Campaign($campaignId);
    $insights = $campaign->getInsights($fields, $params);

    return $insights;
  }

}
